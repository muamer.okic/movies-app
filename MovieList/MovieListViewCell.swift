//
//  MovieListViewCell.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/8/20.
//

import UIKit

class MovieListViewCell: UITableViewCell {

    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var movieDate: UILabel!
    @IBOutlet weak var moviePoster: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMovieCell(movie : Movie) {
        movieName.text = movie.title
        movieDate.text = movie.release_date
        movieDescription.text = movie.overview
        if(movie.poster_path != nil) {
            let s = "https://image.tmdb.org/t/p/w92" + movie.poster_path!
            setImage(from: s, ivImage: moviePoster)
        }
        
    }
}

func setImage(from url: String, ivImage : UIImageView) {
    guard let imageURL = URL(string: url) else { return }

        // just not to cause a deadlock in UI!
    DispatchQueue.global().async {
        guard let imageData = try? Foundation.Data(contentsOf: imageURL) else { return }

        let image = UIImage(data: imageData)
        DispatchQueue.main.async {
            ivImage.image = image
        }
    }
}






