//
//  MovieRequest.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/9/20.
//

import Foundation
import UIKit

enum MovieError : Error{
    case noDataAvalible
    case canNotProcessData
}

public class MovieRequest {
    let resourceURL:URL
    let API_KEY = "2696829a81b1b5827d515ff121700838"
    
    init (query: String, page: Int){
        let queryStr = query.replacingOccurrences(of: " ", with: "%20").lowercased()
        let resourceString = "https://api.themoviedb.org/3/search/movie?api_key=\(API_KEY)&query=\(queryStr)&page=\(String(page))"
        guard let  url = URL(string: resourceString) else {fatalError()}
        self.resourceURL = url
       
    }
    
    func getMovies(completion: @escaping(Result<MovieResponse, MovieError>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourceURL) {data, _, _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvalible))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let moviesResponse = try decoder.decode(MovieResponse.self, from: jsonData)
                let movies = moviesResponse
                completion(.success(movies))
            }
            catch {
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
}
