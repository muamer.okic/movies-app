//
//  Movie.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/9/20.
//

import Foundation

struct Movie : Decodable{
    var title : String?
    var release_date : String?
    var overview : String?
    var poster_path : String?
}

struct MovieResponse : Decodable {
    var page : Int
    var total_results : Int
    var total_pages : Int
    var results:[Movie]
    
}
