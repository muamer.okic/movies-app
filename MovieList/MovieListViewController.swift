//
//  MovieListViewController.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/8/20.
//

import UIKit
import Foundation

class MovieListViewController: UIViewController {

    
    @IBOutlet weak var moviesTable: UITableView!
    
    var movieResponse : MovieResponse?  = nil
    var movieList = [Movie]() {
        didSet {
            DispatchQueue.main.async {
                self.moviesTable.reloadData()
            }
        }
    }
    var query : String = ""
    var page = 1
    var maxPages = 1
    var indicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        self.moviesTable.rowHeight = 300
        self.moviesTable.dataSource = self
        self.moviesTable.delegate = self
        
        super.viewDidLoad()

        
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x:0,y: -1,width: 60,height: 60))
        indicator.style = UIActivityIndicatorView.Style.large
        indicator.color = .white
        indicator.center = self.view.center
        indicator.backgroundColor = .darkGray
        self.view.addSubview(indicator)
    }
    
    func requestMovies(query : String, page : Int) {
        let movieRequest = MovieRequest(query: query, page: page)
        movieRequest.getMovies { [weak self] result in
            switch result {
            case .failure(let error) :
                print(error)
            case .success(let movies) :
                if(movies.total_results > 0){
                    DispatchQueue.main.async {
                        self?.movieList.append(contentsOf: movies.results)
                        self?.moviesTable.reloadData()
                        self?.indicator.stopAnimating()
                        self?.indicator.hidesWhenStopped = true
                    }
                }
            }
        }
    }
    
}

extension MovieListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = movieList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListCell") as! MovieListViewCell
        cell.setMovieCell(movie: movie)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moviesTable.deselectRow(at: indexPath, animated: true)
    }

    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
                        if page < maxPages {
                            activityIndicator()
                            indicator.startAnimating()
                            page = page + 1
                            requestMovies(query: query, page: page)
                        }
                        else {
                            indicator.stopAnimating()
                            indicator.hidesWhenStopped = true
                        }
        }
    }
    
    
    
}




