//
//  Data.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/6/20.
//

import Foundation
import CoreData
import UIKit

public class Data {
    static var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    static func fetchSuggestions() -> [Suggestion] {
        var sugg : [Suggestion] = []
        //var suggestions : [String] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Suggestion")
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        do {
            sugg = try context.fetch(request) as! [Suggestion]
        }
        catch {
            print(error)
        }
        
        return sugg
    }
    
    static func insertSuggestion(id : NSNumber, suggestion : String) {
        Suggestion.insert(id: id, suggestion: suggestion, context: context)
        do {
            try context.save()
        }
        catch {
            print(error)
        }
    }
    
    static func deleteFirst(suggestion : Suggestion) {
        context.delete(suggestion as NSManagedObject)
        do {
        try context.save()
        }
        catch {
            print(error)
        }
    }
}
