//
//  Suggestion.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/6/20.
//

import Foundation
import CoreData

@objc(Suggestion)
public class Suggestion : NSManagedObject {
    @NSManaged var id : NSNumber
    @NSManaged var suggestion : String?
    
    public static func insert(id: NSNumber, suggestion: String, context: NSManagedObjectContext) {
        let sugg = Suggestion(context: context)
        sugg.id = id
        sugg.suggestion = suggestion
    }
}


