//
//  SuggestionsTableViewCell.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/6/20.
//

import UIKit

class SuggestionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var suggestion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSuggestionCell(suggestion: String) {
        self.suggestion.text = suggestion
    }

}
