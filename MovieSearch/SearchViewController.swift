//
//  ViewController.swift
//  MovieApp
//
//  Created by Muamer Okic on 10/6/20.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var suggestionsTable: UITableView!
    
    var suggestionsList : [Suggestion] = []
    var filteredSuggestionsList : [Suggestion] = []
    var searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        suggestionsList = Data.fetchSuggestions()
        //self.suggestionsTable.tableHeaderView = searchController.searchBar
        navigationItem.searchController = searchController
        self.suggestionsTable.dataSource = self
        self.suggestionsTable.delegate = self
        
        suggestionsTable.isHidden = true
        suggestionsTable.rowHeight = 35
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        super.viewDidLoad()
        
    }
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    var isFiltering: Bool {
      return !isSearchBarEmpty
    }
    
    var isAlreadyInSuggestionList : Bool{
        for s in suggestionsList {
            if s.suggestion?.lowercased() == searchController.searchBar.text!.lowercased() {
                return true;
            }
        }
        return false;
    }
    
    
    func requestMovies(query : String, page : Int) {
        let movieRequest = MovieRequest(query: query, page: page)
        movieRequest.getMovies { [weak self] result in
            switch result {
            case .failure(let error) :
                print(error)
            case .success(let movies) :
                if(movies.total_results > 0){
                    DispatchQueue.main.async {
                        let listVC = self?.storyboard?.instantiateViewController(identifier: "MovieListViewController") as! MovieListViewController
                        listVC.movieList = movies.results
                        listVC.query = query
                        listVC.maxPages = movies.total_pages
                        listVC.page = 1
                        self?.navigationController?.pushViewController(listVC, animated: true)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self?.showAlert()
                    }
                }
            }
        }
    }

}

//MARK -- TABLE VIEW

extension SearchViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredSuggestionsList.count
        }
        return suggestionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let suggestion : String
        suggestion = isFiltering ? filteredSuggestionsList[indexPath.row].suggestion! : suggestionsList[indexPath.row].suggestion!
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionsTableCell") as! SuggestionsTableViewCell
        cell.setSuggestionCell(suggestion: suggestion)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        suggestionsTable.deselectRow(at: indexPath, animated: true)
        let query = isFiltering ? filteredSuggestionsList[indexPath.row].suggestion! : suggestionsList[indexPath.row].suggestion!
        requestMovies(query: query, page: 1)
        //var mList : [Movie] = []
        }
       
    }
    

//MARK -- SEARCH VIEW

extension SearchViewController : UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String) {
      filteredSuggestionsList = suggestionsList.filter { (sugg: Suggestion) -> Bool in
        return sugg.suggestion!.lowercased().contains(searchText.lowercased())
      }
      
      suggestionsTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if(!isAlreadyInSuggestionList) {
        if(suggestionsList.count < 10) {
            if(suggestionsList.count == 0) {
                Data.insertSuggestion(id: 0, suggestion: searchBar.text!)
            }
            else {
                Data.insertSuggestion(id: NSNumber(value: suggestionsList[0].id.int32Value + 1), suggestion: searchBar.text!)
            }
        }
        else {
            Data.insertSuggestion(id: NSNumber(value: suggestionsList[0].id.int32Value + 1), suggestion: searchBar.text!)
            Data.deleteFirst(suggestion: suggestionsList[suggestionsList.count - 1])
        }
        
        suggestionsList = Data.fetchSuggestions()
        suggestionsTable.reloadData()
        }
        let query =  searchBar.text!
        searchController.searchBar.text?.removeAll()
        DispatchQueue.main.async {
            self.requestMovies(query:query, page: 1)
        }
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        suggestionsTable.isHidden = true
    }
    
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        suggestionsTable.isHidden = false
        return true;
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Search failed!", message: "No movies were found.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        searchController.searchBar.text?.removeAll()
        self.present(alert, animated: true, completion: nil)
    }

}








